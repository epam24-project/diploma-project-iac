provider "aws" {
  default_tags {
    tags = {
      Deployment = "Terraform"
    }
  }
}

data "aws_eks_cluster" "eks_cl" {
  name = local.cluster_name
  depends_on = [
    aws_eks_cluster.eks_cl
  ]
}

data "aws_eks_cluster_auth" "eks_cl_auth" {
  name = aws_eks_cluster.eks_cl.name
  depends_on = [
    aws_eks_cluster.eks_cl
  ]
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks_cl.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks_cl.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks_cl_auth.token
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.eks_cl.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks_cl.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.eks_cl_auth.token
  }
}
