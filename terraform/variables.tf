locals {
  cluster_name        = "eks-cl"
  node_group_name     = "eks-wg"
  instance_types      = ["t3.small"]
  min_size            = 1
  max_size            = 4
  desired_size        = 2
  cluster_iam_role    = "eks-cluster-iam-role"
  node_group_iam_role = "eks-node-group-iam-role"
  eks_vpc_name        = "eks-vpc"
  eks_ig_name         = "eks-ig"
  rds_dev_name        = "csdbdev"
  rds_dev_user        = "rds_dev_user"
  rds_dev_pass        = "rds-dev-pass"
  rds_prod_name       = "csdb"
  rds_prod_user       = "rds_prod_user"
  rds_prod_pass       = "rds-prod-pass"
}
