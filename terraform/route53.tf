resource "aws_route53_zone" "primary_zone" {
  name = "covidlo.xyz"
}

resource "aws_route53_zone" "dev_zone" {
  name = "dev.covidlo.xyz"
}
