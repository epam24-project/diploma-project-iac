output "eks_cluster_arn" {
  description = "EKS cluster arn"
  value       = aws_eks_cluster.eks_cl.arn
  depends_on = [
    aws_eks_cluster.eks_cl
  ]
}

output "eks_cluster_endpoint" {
  value = aws_eks_cluster.eks_cl.endpoint
  depends_on = [
    aws_eks_cluster.eks_cl
  ]
}

output "eks_cluster_certificate_authority_data" {
  value = aws_eks_cluster.eks_cl.certificate_authority[0].data
  depends_on = [
    aws_eks_cluster.eks_cl
  ]
}

output "rds_dev_endpoint" {
  value = aws_db_instance.rds_dev.endpoint
  depends_on = [
    aws_db_instance.rds_dev
  ]
}

# output "rds_prod_endpoint" {
#   value = aws_db_instance.rds_prod.endpoint
#   depends_on = [
#     aws_db_instance.rds_prod
#   ]
# }
