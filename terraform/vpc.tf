resource "aws_vpc" "main_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = local.eks_vpc_name
  }
}

resource "aws_internet_gateway" "main_ig" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = local.eks_ig_name
  }
}

resource "aws_route" "route_to_main_ig" {
  route_table_id         = aws_vpc.main_vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main_ig.id
}

resource "aws_subnet" "sn1" {
  vpc_id                                      = aws_vpc.main_vpc.id
  cidr_block                                  = "10.0.1.0/24"
  availability_zone                           = "eu-central-1a"
  map_public_ip_on_launch                     = true
  enable_resource_name_dns_a_record_on_launch = true
}

resource "aws_subnet" "sn2" {
  vpc_id                                      = aws_vpc.main_vpc.id
  cidr_block                                  = "10.0.2.0/24"
  availability_zone                           = "eu-central-1b"
  map_public_ip_on_launch                     = true
  enable_resource_name_dns_a_record_on_launch = true
}

resource "aws_route_table_association" "r1" {
  subnet_id      = aws_subnet.sn1.id
  route_table_id = aws_vpc.main_vpc.main_route_table_id
}

resource "aws_route_table_association" "r2" {
  subnet_id      = aws_subnet.sn2.id
  route_table_id = aws_vpc.main_vpc.main_route_table_id
}
