resource "aws_db_subnet_group" "main_db_sg" {
  name = "sg-for-rds"
  subnet_ids = [
    aws_subnet.sn1.id,
    aws_subnet.sn2.id
  ]
}

resource "aws_db_instance" "rds_dev" {
  identifier            = local.rds_dev_name
  engine                = "postgres"
  instance_class        = "db.t3.micro"
  db_subnet_group_name  = aws_db_subnet_group.main_db_sg.name
  username              = local.rds_dev_user
  password              = local.rds_dev_pass
  allocated_storage     = 20
  max_allocated_storage = 0
  publicly_accessible   = true
  storage_type          = "gp2"
  skip_final_snapshot   = true
  vpc_security_group_ids = [
    aws_security_group.rds_sg.id
  ]
  depends_on = [
    aws_db_subnet_group.main_db_sg
  ]
}

# resource "aws_db_instance" "rds_prod" {
#   identifier            = local.rds_prod_name
#   engine                = "postgres"
#   instance_class        = "db.t3.micro"
#   db_subnet_group_name  = aws_db_subnet_group.main_db_sg.name
#   username              = local.rds_prod_user
#   password              = local.rds_prod_pass
#   allocated_storage     = 20
#   max_allocated_storage = 0
#   storage_type          = "gp2"
#   skip_final_snapshot   = true
#   vpc_security_group_ids = [
#     aws_security_group.rds_sg.id
#   ]
#   depends_on = [
#     aws_db_subnet_group.main_db_sg
#   ]
# }
