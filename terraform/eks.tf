resource "aws_key_pair" "wg_nodes_key" {
  key_name   = "ec2-access-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC2fYzz6Qhpivd2BZwe+RV4ZM5VDcj2PPW9O0tOcFkOVhs3XP5IOs6g/n6eDacNQx8c2rvK6/1HRlG6t6Ako5GzaZ4NoB1NjnfwFtyCnIusezAdSKfvzhU7EWAOM6qQi3em/avOXkdP+YQtb5XIWOCGVSnjbcywYUnZNMYADZOSxOGouwbCjqjUdeMlX3KkEDhncCzu+Borpfh3Vg/YbxvwD9ThcGPSa/+UIqFRfhVqP/99BAfNueNYe8Plbyj+IZqu+n6BmIJRicPG2vAZq1bn4gAOWNsd30QXOkrGHa6XPU4cF6qIVM53CUS7sO3s5FHujgxBNdf8oGylW8BSBxZCgqmHNYajQxw1DgPGGUOY65OcOHGxzjyPWyr4bAslqF4ubud6zGLU1MnmXPbKDxW1h1/hSio6g4EU0wDS3I6a8z9hG2FRTfaqpM2ckn79zQwX7hZdeja30Tat+b9v15HFNzzK5SrlHvcHRAhKBFfPYBGPHkwc6ezCni2ccp27PUU="
}

resource "aws_eks_cluster" "eks_cl" {
  name     = local.cluster_name
  role_arn = aws_iam_role.eks_cl_iam.arn
  version  = "1.22"

  vpc_config {
    subnet_ids = [
      aws_subnet.sn1.id, aws_subnet.sn2.id
    ]
    security_group_ids = [
      aws_security_group.eks_cl_sg.id
    ]
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy
  ]
}

resource "aws_eks_node_group" "eks_wg" {
  cluster_name    = aws_eks_cluster.eks_cl.name
  node_group_name = local.node_group_name
  instance_types  = local.instance_types
  node_role_arn   = aws_iam_role.eks_wg_iam.arn
  subnet_ids      = [aws_subnet.sn1.id, aws_subnet.sn2.id]

  scaling_config {
    desired_size = local.desired_size
    max_size     = local.max_size
    min_size     = local.min_size
  }

  update_config {
    max_unavailable = 1
  }

  remote_access {
    ec2_ssh_key = aws_key_pair.wg_nodes_key.id
  }

  depends_on = [
    aws_key_pair.wg_nodes_key,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy
  ]
}

resource "aws_eks_addon" "coredns" {
  addon_name        = "coredns"
  addon_version     = "v1.8.7-eksbuild.1"
  resolve_conflicts = "OVERWRITE"
  cluster_name      = aws_eks_cluster.eks_cl.name
}

resource "aws_eks_addon" "kube-proxy" {
  addon_name        = "kube-proxy"
  addon_version     = "v1.22.6-eksbuild.1"
  resolve_conflicts = "OVERWRITE"
  cluster_name      = aws_eks_cluster.eks_cl.name
}

resource "aws_eks_addon" "vpc-cni" {
  addon_name        = "vpc-cni"
  addon_version     = "v1.11.0-eksbuild.1"
  resolve_conflicts = "OVERWRITE"
  cluster_name      = aws_eks_cluster.eks_cl.name
}

resource "kubernetes_config_map" "aws-auth" {
  data = {
    "mapRoles" = <<-EOT
            - groups:
              - system:bootstrappers
              - system:nodes
              rolearn: arn:aws:iam::834839218937:role/eks-node-group-iam-role
              username: system:node:{{EC2PrivateDNSName}}
        EOT
    "mapUsers" = <<-EOT
            - groups:
              - system:masters
              userarn: arn:aws:iam::834839218937:user/admin
              username: admin
        EOT
  }

  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }
  depends_on = [
    aws_eks_cluster.eks_cl
  ]
}

resource "helm_release" "cert_manager" {
  chart            = "cert-manager"
  repository       = "https://charts.jetstack.io"
  name             = "cert-manager"
  create_namespace = true
  namespace        = "certmgr"
  set {
    name  = "installCRDs"
    value = "true"
  }
  depends_on = [
    aws_eks_node_group.eks_wg,
    aws_eks_addon.coredns,
    aws_eks_addon.vpc-cni
  ]
}

resource "helm_release" "ingress_nginx" {
  name             = "ingress-nginx"
  namespace        = "ingress"
  create_namespace = true
  chart            = "ingress-nginx"
  version          = "4.0.16"
  repository       = "https://kubernetes.github.io/ingress-nginx"

  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-backend-protocol"
    value = "tcp"
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-cross-zone-load-balancing-enabled"
    value = "true"
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-type"
    value = "nlb"
  }

  depends_on = [
    aws_eks_node_group.eks_wg,
    helm_release.cert_manager,
    aws_eks_addon.coredns,
    aws_eks_addon.vpc-cni
  ]
}

resource "helm_release" "prometheus" {
  chart            = "kube-prometheus-stack"
  repository       = "https://prometheus-community.github.io/helm-charts"
  name             = "kube-prometheus-stack"
  create_namespace = true
  namespace        = "monitoring"
  depends_on = [
    aws_eks_node_group.eks_wg,
    aws_eks_addon.coredns,
    aws_eks_addon.vpc-cni
  ]
}
